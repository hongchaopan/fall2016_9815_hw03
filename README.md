# Fall2016 MTH9815 Homework 02 Team 16

Hi Alain and KC, this is my work of the final project of Python part.

## Parser
#### Run the script by command line:

python fall2016_9815_hw03.py -a <path to acquisition file> -p <path to performance file>


## main_analyzer
#### Contains all the solutions of 6 questions
#### input: merged data of Acquisition file and Performance file

**Question number**     |       ** Descriptions**
--------------------    |   ----------------------
*Question1*               |   Input: Keep newest data of same "LOAN_ID" in merged data; Output: Total Current UPB per Channel (R, B, C)
*Question2*               |   Input: Keep newest data of same "LOAN_ID" in merged data; Output: The number of loans, current loans and delinquencies per state
*Question3*               |   Input: Keep newest data of same "LOAN_ID" in merged data; Output: The number of loans, current loans and delinquencies per Channel
*Question4*               |   Input: Removed 'OTHER' in the newest data of removing redudant data of same "LOAN_ID" in merged data; Output: Biggest seller based on number of loans
*Question5*               |   Input: Removed 'OTHER' in the newest data of removing redudant data of same "LOAN_ID" in merged data; Output: Biggest seller based on original UPB
*Question6*               |   Input: merged data; Output: FICO score and Default Rate per Channel





