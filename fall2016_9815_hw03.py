# Python scripts for fall 2016 MTH9815 homework 03
# Copyright (c) Hongchao Pan 2016 Reserved

from __future__ import absolute_import, print_function, division
import pandas as pd
import numpy as np
import argparse
from numpy import matrix
import time

# Define function to extract dat
def read_data(filename1, filename2):
    '''

    :param filename1: file name of the Acquisition
    :param filename2: file name of the Performance
    :return: pandas dataframe
    '''
    # Add columns name
    acq_col = ["LOAN_ID", "ORIG_CHN", "Seller.Name", "ORIG_AMT", "CSCORE_B", "STATE"]
    perf_col = ["LOAN_ID", "Servicer.Name", "LAST_UPB", "Delq.Status"]
    acq = pd.read_csv(filename1, delimiter='|', header=None, usecols=[0, 1, 2, 4, 12, 18], names=acq_col)
    perf = pd.read_csv(filename2, delimiter='|', header=None, usecols=[0, 2, 4, 10], names=perf_col)

    # Merge data by "LOAN_ID"
    data=pd.merge(acq,perf,on="LOAN_ID")

    return data

def main_analyzer(data):
    '''
    Analyze the data
    :param data: merged data
    :return:
    '''
    # Q1)
    # Check the data
    print("Check the merged data")
    print(data[["LOAN_ID", "ORIG_CHN", "LAST_UPB"]].head(20))

    # Remove the duplicate data, only keep the lateset data
    print("Remove the duplicate data, only keep the lateset data")
    inde = [True] * data.shape[0]
    for i in range(data.shape[0] - 1):
        if data["LOAN_ID"][i] == data["LOAN_ID"][i + 1]:
            inde[i] = False
    data2 = data[inde]
    # Check the data2
    print("Check removing the redundant data")
    print(data2[["LOAN_ID","ORIG_CHN","LAST_UPB"]].head())

    # Calculate the UPB per Channel
    # Get the unique channel value
    channel = data2.ORIG_CHN.unique()
    #print("The unique value of Channel are:",channel)

    UPB = []

    for i in channel:
        ind = [j == i for j in data2["ORIG_CHN"]]
        UPB.append(data2["LAST_UPB"][ind].sum())

    UPB_per_channel = pd.DataFrame(matrix(UPB), columns=channel, index=["UPB"])
    print("********************")
    print("Answer of question 1")

    print("The current UPB per Channel is:")
    print(UPB_per_channel)
    print("********************")
    # Q2
    # Calculate the distribution of loans, current loans and delinquencies per state
    # Get the unique value in STATE
    state = data2.STATE.unique()
    loan, current_loan, deliq = [], [], []

    for i in state:
        ind = [j == i for j in data2["STATE"]]
        loan.append([data2["ORIG_AMT"][ind].sum()])
        current_loan.append(data2["LAST_UPB"][ind].sum())

        # Get the number of delinquencies
        sum = 0
        delq = data2["Delq.Status"][ind]
        for k in delq:
            if k != 'X':
                sum += int(k)  # type of k is str
        deliq.append(sum)

    #print(deliq)

    dist_per_state = pd.DataFrame(matrix([loan, current_loan, deliq]), columns=state,
                                  index=["Loan", "Current Loan", "Delinquencies"])
    print("********************")
    print("Answer of question 2")
    print("The distribution of loans, current loans and delinquencies per State is:")
    print(dist_per_state)
    print("********************")
    # Q3)
    # Calculate the distribution of loans, current loans and delinquencies per Channel
    loan, current_loan, deliq = [], [], []

    for i in channel:
        ind = [j == i for j in data2["ORIG_CHN"]]
        loan.append([data2["ORIG_AMT"][ind].sum()])
        current_loan.append(data2["LAST_UPB"][ind].sum())

        # Get the number of delinquencies
        delq = data2["Delq.Status"][ind]
        for k in delq:
            if k != 'X':
                sum += int(k)  # type of k is str
        deliq.append(sum)

    dist_per_channel = pd.DataFrame(matrix([loan, current_loan, deliq]), columns=channel,
                                    index=["Loan", "Current Loan", "Delinquencies"])
    print("********************")
    print("Answer of question 3")
    print("The distribution of loans, current loans and delinquencies per Channel is:")
    print(dist_per_channel)
    print("********************")

    # Modify data: remove the data of "OTHER" in Servicer.name and "Seller.name"
    # data3=data2[data2["Servicer.Name"]!="OTHER"]
    data3 = data2[data2["Seller.Name"] != "OTHER"]

    # Q4)
    # Get the largest seller by number of loans
    # Update the seller list (without 'OTHER')
    seller = data3["Seller.Name"].unique()
    # print(seller)
    loan = []
    for i in seller:
        loan.append(data3[data3["Seller.Name"] == i].shape[0])  # Get the number of loans of the firms
    # print(loan)
    # Get the max number
    seller_loan = pd.DataFrame(loan, columns=["Number of Loan"], index=seller)
    # print(seller_loan)
    # max_seller=seller_loan.idxmax()
    # max_loan=max(loan)
    max_seller = seller_loan.loc[seller_loan['Number of Loan'].idxmax()]
    print("********************")
    print("Answer of question 4")
    print("The biggest seller by number of Loan is: ")
    print(max_seller)
    print("********************")
    # Q5)
    # Get the largest seller by the Original UPB
    # Update the seller list (without 'OTHER')
    Orig_UPB = []
    for i in seller:
        Orig_UPB.append(data3[data3["Seller.Name"] == i]["ORIG_AMT"].sum())  # Get the number of loans of the firms
    print(Orig_UPB)
    seller_UPB = pd.DataFrame(Orig_UPB, columns=["Original UPB"], index=seller)
    max_seller_UPB = seller_UPB.loc[seller_UPB["Original UPB"].idxmax()]
    print("********************")
    print("Answer of question 5")
    print("The biggest seller by the Original UPB is :")
    print(max_seller_UPB)
    print("********************")
    # Q6)
    # Create the FICO band
    #print(data3["CSCORE_B"].head())

    score = np.zeros(7)
    for i in data["CSCORE_B"]:
        if (0 <= i < 620):
            score[0] += 1
        elif (620 <= i < 660):
            score[1] += 1
        elif (660 <= i < 700):
            score[2] += 1
        elif (700 <= i < 740):
            score[3] += 1
        elif (740 <= i < 780):
            score[4] += 1
        elif (780 < i):
            score[5] += 1
    sum = 0
    for j in range(6):
        sum += score[j]
    score[6] = data.shape[0] - sum
    FICO = pd.DataFrame(matrix(score),
                        columns=["[0-620)", "[620-660)", "[660-700)", "[700-740)", "[740-780)", "[780+)", "Missing"],
                        index=["FICO score"])
    print("********************")
    print("Answer of question 6")
    print(FICO)

    # Calculate the Default rate per Channel
    default_rate = []

    for i in channel:
        ind = [j == i for j in data["ORIG_CHN"]]

        deliq = data["Delq.Status"][ind]
        default, total = 0, 0
        for k in deliq:
            if k != 'X':
                total += 1
                if int(k) >= 4:
                    default += 1
        default_rate.append(default / total)
    print(default_rate)
    default_channel = pd.DataFrame(matrix(default_rate), columns=channel, index=["Default rate per Channel"])
    print("The default rate per Channel is:")
    print(default_channel)
    print("********************")

if __name__ == "__main__":

    start=time.time()
    # Using argparse package to parse the data
    # python fall2016_9815_hw03.py -a <path to acquisition file> -p <path to performance file>
    parser = argparse.ArgumentParser(description='Using argparse package to parse data')
    parser.add_argument('-a', type = str)
    parser.add_argument('-p', type = str)
    print("Parsing data")
    args = parser.parse_args()

    # Merged the data
    data=read_data(args.a,args.p)

    # Run the analyer
    main_analyzer(data)

    end=time.time()
    print("Running time: ",(end-start))

